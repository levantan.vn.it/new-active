<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    protected $table = 'transportations';

    public function fk_province()
    {
    	return $this->hasOne(Province::class, 'id', 'province_id');
    }

    public function fk_transportation_areas()
    {
    	return $this->hasMany(TransportationArea::class, 'transport_id');
    }
}
