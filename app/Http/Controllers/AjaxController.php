<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class AjaxController extends Controller
{
    public function getDistrict(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('province_id')){
                $districts = \App\District::where('province_id', $request->province_id)->get();
                $html = '<option value="">'.__('— Choose district —').'</option>';
                foreach($districts as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }

    public function getWard(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('district_id')){
                $wards = \App\Ward::where('district_id', $request->district_id)->get();
                $html = '<option value="">'.__('— Choose ward —').'</option>';
                foreach($wards as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }
}


