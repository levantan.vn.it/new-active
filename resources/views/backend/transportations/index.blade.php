@extends('backend.layouts.app')
@section('css')
<script type="text/javascript">
    var base_url = "{!! url('') !!}/";
</script>
<style type="text/css">
    .card-header{
        padding-top: 10px;
        padding-left: 15px;
    }
    .table th {
        font-weight: 600;
        border-bottom: 1px solid rgba(0,0,0,0.07);
        color: #4d627b;
        border-top: none;
        width: 100%
    }
    .wd-100{
        width: 100%
    }
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-sm-12">
        <button class="btn btn-rounded btn-info" data-toggle="modal" data-target="#createAreaModal">{{__("Add shipping")}}</button>
    </div>
</div>

<br>

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="row">
    <div class="col-lg-4">
        <div class="card">
            <h5 class="card-header pb-0">{{__('Config shipping fee')}}</h5>
            <div class="card-body">
                <form action="{{ route('admin.transportation.setting') }}" method="POST" id="settingForm">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Default shipping rates')}}</label>
                        <div class="input-group">
                            <input type="text" name="default_shipping_price" value="{{ number_format(get_settings('default_shipping_price')->value) }}" class="form-control" onkeyup="make_money_format(event)"{!! get_settings('default_shipping_price')->disabled ? ' disabled' : null !!} required>
                            <span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-from-label">Refuse to shipping</label>
                        <div class="col-md-7">
                            <label class="aiz-switch aiz-switch-success mb-0">
                                <input type="checkbox" name="disabled" id="disabledDefaultShipping"{!! get_settings('default_shipping_price')->disabled ? ' checked' : null !!}>
                                <span></span>
                            </label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label>{{__('Free delivery for minimum value orders')}}</label>
                        <div class="input-group">
                            <input type="text" name="free_order_mim" value="{{ number_format(get_settings('free_order_mim')->value) }}" class="form-control" onkeyup="make_money_format(event)"{!! get_settings('free_order_mim')->disabled ? ' disabled' : null !!} required>
                            <span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('Apply District')}}</label>
                        <?php
                        $apply_district = get_settings('apply_district')->value;
                        if(!empty($apply_district)){
                            $apply_district = unserialize($apply_district);
                        }else{
                            $apply_district = [0];
                        }

                        ?>
                        <div class="input-group" style="width: 100%">
                            <select class="custom-select form-control apply_district" multiple name="apply_district[]">
                                <option value="">{{__('— Choose district —')}}</option>
                                @php
                                    $districts = App\District::get();
                                @endphp
                                @foreach($districts as $district)
                                    <option {{in_array($district->id,$apply_district)?'selected':''}} value="{{ $district->id }}">{{ $district->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-5 col-from-label">Disable</label>
                        <div class="col-md-7">
                            <label class="aiz-switch aiz-switch-success mb-0">
                                <input type="checkbox" name="disabled_free_order_mim" id="disabledFree"{!! get_settings('free_order_mim')->disabled ? ' checked' : null !!}>
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{__('Fast Delivery')}}</label>
                            <input type="text" name="price_shipping_fast" value="{{ number_format(get_settings('price_shipping_fast')->value) }}" class="form-control" onkeyup="make_money_format(event)">
                    </div>
                </form>

                <p>{{__("Areas that are not pre-installed will apply default shipping rates.")}}</p>
                <p>{{__("If the Refuse to shipping option is enabled, shipping areas not pre-installed will not be applied.")}}</p>
            </div>
            <div class="card-footer">
                <button class="btn btn-success" form="settingForm">{{__("Save")}}</button>
            </div>
        </div>
        <div class="card">
            <h5 class="card-header pb-0">{{__("Instruction")}}</h5>
            <div class="card-body">
                <p>{{__("You can choose the shipping fee for each specific Province and District")}}</p>
            </div>
            <div class="card-footer">
                {{-- <a href="#" class="btn btn-info">Xem hướng dẫn</a> --}}
            </div>
        </div>
    </div>

    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
            @foreach($data as $item)
                <table class="table table-responsive">
                    <thead>
                        <tr>
                            <th colspan="2">
                                {{ optional($item->fk_province)->name }} - <a href="#" onclick="destroyItem(event, '{{ route('admin.transportation.destroy', $item->id) }}');">{{__('Delete area')}}</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td width="50%" class="py-3">
                                <a data-toggle="collapse" href="#collapse-{{ $item->id }}">{{ $item->name }}</a>
                            </td>
                            <td width="20%" class="py-3">
                                @if($item->price > 0)
                                    {{ number_format($item->price, 0, ',', '.')}} <span class="sb sb-vnd"></span>
                                @else
                                    {{__('Free')}}

                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="collapse" id="collapse-{{ $item->id }}">
                    <form action="{{ route('admin.transportation.update', $item->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>{{__('Method Name')}} <span class="required">*</span></label>
                                    <input type="text" name="name" value="{{ $item->name }}" class="form-control" required>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>{{__('General fee')}} <span class="required">*</span></label>
                                            <div class="input-group">
                                                <input type="text" name="price" value="{{ number_format($item->price) }}" class="form-control" onkeyup="make_money_format(event);" required>
                                                <span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
                                            </div>
                                            <span class="help-block">{{__('Applies to all district in the area')}}.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <a class="btn btn-rounded btn-info" data-toggle="modal" data-target="#createAddressModal-{{ $item->id }}"><i class="fa fa-plus fa-fw"></i>{{__("Add Special Shipping")}}</a>
                                </div>


                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>{{__('District')}}</th>
                                             <th>{{__('Ward')}}</th>
                                            <th>{{__('Specific fees')}}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($item->fk_transportation_areas as $area)
                                        <tr{!! $area->denied ? ' class="text-danger"' : null !!}>
                                            <td>
                                                <input type="hidden" name="district_id[{{ $area->id }}]" value="{{ $area->district_id }}">
                                                {{ optional($area->fk_district)->name }}
                                            </td>
                                            <td>
                                                <input type="hidden" name="ward_id[{{ $area->id }}]" value="{{ $area->ward_id }}">
                                                <p style="width: 200px">{{ optional($area->fk_ward)->name }}</p>

                                            </td>
                                            <td>
                                                <div class="input-group" style="width: 200px">
                                                    <div class="input-group-prepend">
                                                        <a style="width: 50px" class="input-group-addon addon-info" onclick="DeleteArea(event, '{{ route('admin.transportation.destroy-area', $area->id) }}');" href="javascript:void(0);" data-toggle="tooltip" title="Delete Area"><i class="las la-trash"></i></a>
                                                    </div>
                                                    <input type="text" name="district_price[{{ $area->id }}]" value="{{ number_format($area->price) }}" class="form-control" onkeyup="make_money_format(event);"{!! $area->denied ? ' readonly' : null !!}>

                                                </div>
                                            </td>
                                            <td class="text-right">
                                                <div class="row" style="width: 200px">
                                                    <div class="col-md-3">
                                                        <label class="aiz-switch aiz-switch-success mb-0">
                                                            <input type="checkbox" name="district_deny[{{ $area->id }}]" onchange="is_deny(event)"{!! $area->denied ? ' checked' : null !!}>
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                    <label class="col-md-9 col-from-label">{{__('Refuse to shipping')}}</label>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <button class="btn btn-success">{{__('Save')}}</button>
                                <button class="btn btn-light" type="button" data-toggle="collapse" href="#collapse-{{ $item->id }}">{{__('Cancel')}}</button>
                            </div>
                        </div>
                    </form>
                    <div class="modal fade" id="createAddressModal-{{ $item->id }}">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">{{__('Add an special shipping')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form class="createAddressForm" action="{{ route('admin.transportation.store-address') }}" method="POST" id="createAddressForm-{{ $item->id }}">
                                        @csrf
                                        <input type="hidden" name="transport_id" value="{{ $item->id }}">
                                        <input type="hidden" name="province_id" value="{{ $item->province_id }}">
                                        <div class="form-group">
                                            <label>{{__('Province')}}: {{ optional($item->fk_province)->name }}</label>

                                        </div>
                                        <div class="form-group">
                                            <label>{{__('District')}} <span class="required">*</span></label>
                                            <select class="custom-select form-control" name="district_id" required onchange="get_wards(event)">
                                                <option value="">{{__('— Choose district —')}}</option>
                                                @php
                                                    $districts = get_districts_by_province($item->province_id);
                                                @endphp
                                                @foreach($districts as $district)
                                                    <option value="{{ $district->id }}">{{ $district->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>{{__('Ward')}}</label>
                                            <select class="custom-select form-control" name="ward_id">
                                            </select>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>{{__('General fee')}} <span class="required">*</span></label>
                                            <div class="input-group">
                                                <input type="text" name="price" value="0" class="form-control" onkeyup="make_money_format(event);" required>
                                                <span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
                                            </div>
                                            <label id="price-error" class="error" for="price" style="display: none;"></label>
                                            <span class="help-block">{{__('Applies to all district in the area.')}}</span>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-success" form="createAddressForm-{{ $item->id }}">{{__('Save')}}</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
@section('modal')
<div class="modal fade" id="createAreaModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Add an area')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('admin.transportation.store') }}" method="POST" id="createAreaForm">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Province')}} <span class="required">*</span></label>
                        <select class="custom-select form-control" name="province_id" required>
                            <option value="">{{__('— Choose province —')}}</option>
                            @foreach($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>{{__('Method Name')}} <span class="required">*</span></label>
                        <input type="text" name="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>{{__('General fee')}} <span class="required">*</span></label>
                        <div class="input-group">
                            <input type="text" name="price" value="0" class="form-control" onkeyup="make_money_format(event);" required>
                            <span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
                        </div>
                        <label id="price-error" class="error" for="price" style="display: none;"></label>
                        <span class="help-block">{{__('Applies to all district in the area.')}}</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" form="createAreaForm">{{__('Save')}}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Cancel')}}</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    {{-- Toastr Notification JS --}}
    <script type="text/javascript" src="{{ asset('public/plugins/toastr/2.1.3/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/plugins/toastr/2.1.3/toastr.config.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/toastr/2.1.3/toastr.min.css') }}">

    {{-- Sweet Alert --}}
    <script src="{{ asset('public/plugins/sweetalert/2v7.0.9/sweetalert.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/plugins/sweetalert/2v7.0.9/sweetalert.min.css') }}">
    <script src="{{ asset('public/js/areas.handler.js') }}"></script>
    <script src="{{ asset('public/plugins/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        $("#disabledDefaultShipping").change(function(){
            if($(this).is(":checked")){
                $("input[name=default_shipping_price]").attr("disabled", true);
            }else{
                $("input[name=default_shipping_price]").attr("disabled", false);
            }
        });
        $("#disabledFree").change(function(){
            if($(this).is(":checked")){
                $("input[name=free_order_mim]").attr("disabled", true);
            }else{
                $("input[name=free_order_mim]").attr("disabled", false);
            }
        });
    </script>
    <script>
        $('.apply_district').select2();
        $("#navOrder").addClass("active").find("ul").addClass("show").find(".transportation").addClass("active");

        $("#createAreaForm").validate();
        function is_deny(event){
            if($(event.target).is(":checked")){
                $(event.target).parents("tr").find(".form-control").attr("readonly", true);
                $(event.target).parents("tr").addClass("text-danger");
            }else{
                $(event.target).parents("tr").find(".form-control").attr("readonly", false);
                $(event.target).parents("tr").removeClass("text-danger");
            }
        }
    </script>
    <script>
        function destroyItem(event, url){
            swal({
                title: 'Are you sure',
                text: "Do you want remove it?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Delete',
                cancelButtonText: 'Cancel'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url : url,
                        type : 'DELETE',
                        success : function (data) {
                            $(event.target).parents("table").remove();
                            toastr.success("<b>" + data.title + "</b><br/>" + data.msg);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            toastr.error(xhr.responseText);
                        }
                    });
                }
            })
        }
        function DeleteArea(event, url){
            swal({
                title: 'Are you sure',
                text: "Do you want remove area?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Delete',
                cancelButtonText: 'Cancel'
            }).then(function(isConfirm) {
                if (isConfirm.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url : url,
                        type : 'DELETE',
                        success : function (data) {
                            $(event.target).parents("tr").remove();
                            toastr.success("<b>" + data.title + "</b><br/>" + data.msg);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            toastr.error(xhr.responseText);
                        }
                    });
                }
            })
        }
        function number_format(number, decimals, dec_point, thousands_sep) {
            number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function(n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '').length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1).join('0');
            }
            return s.join(dec);
        };
        var make_money_format = function(event) {
            var val = event.target.value;
            if (val != "") val = number_format(val);
            $(event.target).val(val);
        };
    </script>
@endsection
