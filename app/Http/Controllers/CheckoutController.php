<?php

namespace App\Http\Controllers;

use App\Utility\PayfastUtility;
use Illuminate\Http\Request;
use Auth;
use App\Category;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\InstamojoController;
use App\Http\Controllers\ClubPointController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\PaytmController;
use App\Order;
use App\BusinessSetting;
use App\Coupon;
use App\CouponUsage;
use App\User;
use App\Address;
use Session;
use App\Utility\PayhereUtility;

class CheckoutController extends Controller
{

    public function __construct()
    {
        //
    }

    //check the selected payment gateway and redirect to that controller accordingly
    public function checkout(Request $request)
    {

        if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $cart = $request->session()->get('cart', collect([]));

            $check_error = false;
            foreach ($cart as $key => $object) {
                if (\App\Product::find($object['id'])->added_by == 'admin') {
                    if ($request['shipping_type_admin'] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_admin'] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        if(empty($request->pickup_point_id_admin)){
                            flash(translate('Select your nearest pickup point.'))->warning();
                            return back();
                        }
                        $object['pickup_point'] = $request->pickup_point_id_admin;
                    }
                } else {
                    if ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        if(empty($request['pickup_point_id_' . \App\Product::find($object['id'])->user_id])){
                            flash(translate('Select your nearest pickup point.'))->warning();
                            return back();
                        }
                        $object['pickup_point'] = $request['pickup_point_id_' . \App\Product::find($object['id'])->user_id];
                    }
                }
            }
        } else {
            flash(translate('Your Cart was empty'))->warning();
            return redirect()->route('home');
        }
        if ($request->payment_option != null) {

            $orderController = new OrderController;
            $orderController->store($request);

            $request->session()->put('payment_type', 'cart_payment');

            if ($request->session()->get('order_id') != null) {
                if ($request->payment_option == 'paypal') {
                    $paypal = new PaypalController;
                    return $paypal->getCheckout();
                } elseif ($request->payment_option == 'stripe') {
                    $stripe = new StripePaymentController;
                    return $stripe->stripe();
                } elseif ($request->payment_option == 'sslcommerz') {
                    $sslcommerz = new PublicSslCommerzPaymentController;
                    return $sslcommerz->index($request);
                } elseif ($request->payment_option == 'instamojo') {
                    $instamojo = new InstamojoController;
                    return $instamojo->pay($request);
                } elseif ($request->payment_option == 'razorpay') {
                    $razorpay = new RazorpayController;
                    return $razorpay->payWithRazorpay($request);
                } elseif ($request->payment_option == 'paystack') {
                    $paystack = new PaystackController;
                    return $paystack->redirectToGateway($request);
                } elseif ($request->payment_option == 'voguepay') {
                    $voguePay = new VoguePayController;
                    return $voguePay->customer_showForm();
                } elseif ($request->payment_option == 'twocheckout') {
                    $twocheckout = new TwoCheckoutController;
                    return $twocheckout->index($request);
                } elseif ($request->payment_option == 'payhere') {
                    $order = Order::findOrFail($request->session()->get('order_id'));

                    $order_id = $order->id;
                    $amount = $order->grand_total;
                    $first_name = json_decode($order->shipping_address)->name;
                    $last_name = 'X';
                    $phone = json_decode($order->shipping_address)->phone;
                    $email = json_decode($order->shipping_address)->email;
                    $address = json_decode($order->shipping_address)->address;
                    $city = json_decode($order->shipping_address)->city;

                    return PayhereUtility::create_checkout_form($order_id, $amount, $first_name, $last_name, $phone, $email, $address, $city);
                } elseif ($request->payment_option == 'payfast') {
                    $order = Order::findOrFail($request->session()->get('order_id'));

                    $order_id = $order->id;
                    $amount = $order->grand_total;

                    return PayfastUtility::create_checkout_form($order_id, $amount);
                } else if ($request->payment_option == 'ngenius') {
                    $ngenius = new NgeniusController();
                    return $ngenius->pay();
                } else if ($request->payment_option == 'iyzico') {
                    $iyzico = new IyzicoController();
                    return $iyzico->pay();
                } else if ($request->payment_option == 'flutterwave') {
                    $flutterwave = new FlutterwaveController();
                    return $flutterwave->pay();
                } else if ($request->payment_option == 'mpesa') {
                    $mpesa = new MpesaController();
                    return $mpesa->pay();
                } elseif ($request->payment_option == 'paytm') {
                    $paytm = new PaytmController;
                    return $paytm->index();
                } elseif ($request->payment_option == 'cash_on_delivery') {
                    $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                    $request->session()->forget('owner_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');

                    flash(translate("Your order has been placed successfully"))->success();
                    return redirect()->route('order_confirmed');
                } elseif ($request->payment_option == 'wallet') {
                    $user = Auth::user();
                    $order = Order::findOrFail($request->session()->get('order_id'));
                    if ($user->balance >= $order->grand_total) {
                        $user->balance -= $order->grand_total;
                        $user->save();
                        return $this->checkout_done($request->session()->get('order_id'), null);
                    }
                } else {
                    $order = Order::findOrFail($request->session()->get('order_id'));
                    $order->manual_payment = 1;
                    $order->save();

                    $request->session()->put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
                    $request->session()->forget('owner_id');
                    $request->session()->forget('delivery_info');
                    $request->session()->forget('coupon_id');
                    $request->session()->forget('coupon_discount');

                    flash(translate('Your order has been placed successfully. Please submit payment information from purchase history'))->success();
                    return redirect()->route('order_confirmed');
                }
            }
        } else {
            flash(translate('Select Payment Option.'))->warning();
            return back();
        }
    }

    //redirects to this method after a successfull checkout
    public function checkout_done($order_id, $payment)
    {
        $order = Order::findOrFail($order_id);
        $order->payment_status = 'paid';
        $order->payment_details = $payment;
        $order->save();

        if (\App\Addon::where('unique_identifier', 'affiliate_system')->first() != null && \App\Addon::where('unique_identifier', 'affiliate_system')->first()->activated) {
            $affiliateController = new AffiliateController;
            $affiliateController->processAffiliatePoints($order);
        }

        if (\App\Addon::where('unique_identifier', 'club_point')->first() != null && \App\Addon::where('unique_identifier', 'club_point')->first()->activated) {
            $clubpointController = new ClubPointController;
            $clubpointController->processClubPoints($order);
        }
        if (\App\Addon::where('unique_identifier', 'seller_subscription')->first() == null || !\App\Addon::where('unique_identifier', 'seller_subscription')->first()->activated) {
            if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                foreach ($order->orderDetails as $key => $orderDetail) {
                    $orderDetail->payment_status = 'paid';
                    $orderDetail->save();
                    if ($orderDetail->product->user->user_type == 'seller') {
                        $seller = $orderDetail->product->user->seller;
                        $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                        $seller->save();
                    }
                }
            } else {
                foreach ($order->orderDetails as $key => $orderDetail) {
                    $orderDetail->payment_status = 'paid';
                    $orderDetail->save();
                    if ($orderDetail->product->user->user_type == 'seller') {
                        $commission_percentage = $orderDetail->product->category->commision_rate;
                        $seller = $orderDetail->product->user->seller;
                        $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price * (100 - $commission_percentage)) / 100 + $orderDetail->tax + $orderDetail->shipping_cost;
                        $seller->save();
                    }
                }
            }
        } else {
            foreach ($order->orderDetails as $key => $orderDetail) {
                $orderDetail->payment_status = 'paid';
                $orderDetail->save();
                if ($orderDetail->product->user->user_type == 'seller') {
                    $seller = $orderDetail->product->user->seller;
                    $seller->admin_to_pay = $seller->admin_to_pay + $orderDetail->price + $orderDetail->tax + $orderDetail->shipping_cost;
                    $seller->save();
                }
            }
        }

        $order->commission_calculated = 1;
        $order->save();

        if (Session::has('cart')) {
            Session::put('cart', Session::get('cart')->where('owner_id', '!=', Session::get('owner_id')));
        }
        Session::forget('owner_id');
        Session::forget('payment_type');
        Session::forget('delivery_info');
        Session::forget('coupon_id');
        Session::forget('coupon_discount');


        flash(translate('Payment completed'))->success();
        return view('frontend.order_confirmed', compact('order'));
    }

    public function get_shipping_info(Request $request)
    {
        if(Auth::check()){
            session()->put('owner_id', Auth::user()->id);
        }

        if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $categories = Category::all();
            return view('frontend.shipping_info', compact('categories'));
        }
        flash(translate('Your cart is empty'))->success();
        return back();
    }

    public function store_shipping_info(Request $request)
    {
        if (Auth::check()) {
            if ($request->address_id == null) {
                flash(translate("Please add shipping address"))->warning();
                return back();
            }
            $address = Address::findOrFail($request->address_id);
            $data['name'] = $address->name;
            $data['email'] = Auth::user()->email;
            $data['address'] = $address->address;
            $data['company'] = $address->company;
            $data['province_id'] = $address->province_id;
            $data['district_id'] = $address->district_id;
            $district_id = $address->district_id;
            $data['ward_id'] = $address->ward_id;
            $ward_id = $address->ward_id;
            $data['city'] = optional($address->fk_province)->name;
            $data['district'] = optional($address->fk_district)->name;
            $data['ward'] = optional($address->fk_ward)->name;
            $data['type'] = $address->type;
            $data['phone'] = $address->phone;
            $data['checkout_type'] = $request->checkout_type;
        } else {
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['address'] = $request->address;
            $data['company'] = $request->company;
            $data['province_id'] = $request->province_id;
            $data['district_id'] = $request->district_id;
            $data['ward_id'] = $request->ward_id;
            $data['city'] = optional($address->fk_province)->name;
            $data['district'] = optional($address->fk_district)->name;
            $data['ward'] = optional($address->fk_ward)->name;
            $data['type'] = $request->type;
            $data['phone'] = $request->phone;
            $data['checkout_type'] = $request->checkout_type;
        }
        $shippingFee = \App\TransportationArea::where('district_id', $district_id)->where('ward_id', $ward_id)->where('denied',1)->first();
        if(!empty($shippingFee)){
            flash(translate("Refuse to shipping address"))->warning();
            return back();
        }else{
            $shippingFee = \App\TransportationArea::where('district_id', $district_id)->whereNull('ward_id')->where('denied',1)->first();
            if(!empty($shippingFee)){
                flash(translate("Refuse to shipping address"))->warning();
                return back();
            }
        }

        $shipping_info = $data;
        $request->session()->put('shipping_info', $shipping_info);

        $subtotal = 0;
        $tax = 0;
        $shipping = $this->getShippingFee();
        foreach (Session::get('cart') as $key => $cartItem) {
            $subtotal += $cartItem['price'] * $cartItem['quantity'];
            $tax += $cartItem['tax'] * $cartItem['quantity'];
            // $shipping += $cartItem['shipping'] * $cartItem['quantity'];
        }

        $total = $subtotal + $tax + $shipping;

        if (Session::has('coupon_discount')) {
            $total -= Session::get('coupon_discount');
        }

        $min_order_free = -1;
        if(!empty(get_settings('apply_district'))){
            $apply_district = get_settings('apply_district')->value;
        }

        if(!get_settings('free_order_mim')->disabled && !empty($apply_district) && in_array(session('shipping_info')['district_id'], unserialize($apply_district))){
            $min_order_free = get_settings('free_order_mim')->value;
        }
        $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->where('ward_id', session('shipping_info')['ward_id'])->where('denied',0)->first();
        if (is_null($shippingFee)){
            $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->whereNull('ward_id')->where('denied',0)->first();
            if (is_null($shippingFee)){
                $shippingFee = \App\Transportation::where('province_id', session('shipping_info')['province_id'])->first();
                if (is_null($shippingFee)){
                    $shippingFee = get_settings('default_shipping_price')->value;
                }else{
                    $shippingFee = $shippingFee->price;
                }
            }
            else{
                $shippingFee = $shippingFee->price;
            }
        }else{
            $shippingFee = $shippingFee->price;
        }

        return view('frontend.delivery_info',compact('total','shipping','min_order_free','shippingFee'));
        // return view('frontend.payment_select', compact('total'));
    }

    public function delivery_info(Request $request)
    {

        $subtotal = 0;
        $tax = 0;
        $shipping = $this->getShippingFee();

        foreach (Session::get('cart') as $key => $cartItem) {
            $subtotal += $cartItem['price'] * $cartItem['quantity'];
            $tax += $cartItem['tax'] * $cartItem['quantity'];
        }
        $total = $subtotal + $tax + $shipping;

        if (Session::has('coupon_discount')) {
            $total -= Session::get('coupon_discount');
        }
        $min_order_free = -1;
        if(!empty(get_settings('apply_district'))){
            $apply_district = get_settings('apply_district')->value;
        }

        if(!get_settings('free_order_mim')->disabled && !empty($apply_district) && in_array(session('shipping_info')['district_id'], unserialize($apply_district))){
            $min_order_free = get_settings('free_order_mim')->value;
        }
        $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->where('ward_id', session('shipping_info')['ward_id'])->where('denied',0)->first();
        if (is_null($shippingFee)){
            $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->whereNull('ward_id')->where('denied',0)->first();
            if (is_null($shippingFee)){
                $shippingFee = \App\Transportation::where('province_id', session('shipping_info')['province_id'])->first();
                if (is_null($shippingFee)){
                    $shippingFee = get_settings('default_shipping_price')->value;
                }else{
                    $shippingFee = $shippingFee->price;
                }
            }
            else{
                $shippingFee = $shippingFee->price;
            }
        }else{
            $shippingFee = $shippingFee->price;
        }

        return view('frontend.delivery_info', compact('total','shipping','min_order_free','shippingFee'));
        // return view('frontend.payment_select', compact('total'));
    }

    public function store_delivery_info(Request $request)
    {
        $request->session()->put('owner_id', $request->owner_id);

        if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
                if (\App\Product::find($object['id'])->added_by == 'admin') {
                    if ($request['shipping_type_admin'] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_admin'] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request->pickup_point_id_admin;
                    }
                } else {
                    if ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request['pickup_point_id_' . \App\Product::find($object['id'])->user_id];
                    }
                }
                return $object;
            });

            $request->session()->put('cart', $cart);

            $cart = $cart->map(function ($object, $key) use ($request) {
                if (\App\Product::find($object['id'])->user_id == $request->owner_id) {
                    $object['shipping'] = getShippingCost($key);
                } else {
                    $object['shipping'] = 0;
                }
                return $object;
            });

            $request->session()->put('cart', $cart);

            $subtotal = 0;
            $tax = 0;
            $shipping = $this->getShippingFee();
            foreach (Session::get('cart') as $key => $cartItem) {
                $subtotal += $cartItem['price'] * $cartItem['quantity'];
                $tax += $cartItem['tax'] * $cartItem['quantity'];
                // $shipping += $cartItem['shipping'] * $cartItem['quantity'];
            }

            $total = $subtotal + $tax + $shipping;

            if (Session::has('coupon_discount')) {
                $total -= Session::get('coupon_discount');
            }

            $min_order_free = -1;
            if(!empty(get_settings('apply_district'))){
                $apply_district = get_settings('apply_district')->value;
            }

            if(!get_settings('free_order_mim')->disabled && !empty($apply_district) && in_array(session('shipping_info')['district_id'], unserialize($apply_district))){
                $min_order_free = get_settings('free_order_mim')->value;
            }
            $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->where('ward_id', session('shipping_info')['ward_id'])->where('denied',0)->first();
            if (is_null($shippingFee)){
                $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->whereNull('ward_id')->where('denied',0)->first();
                if (is_null($shippingFee)){
                    $shippingFee = \App\Transportation::where('province_id', session('shipping_info')['province_id'])->first();
                    if (is_null($shippingFee)){
                        $shippingFee = get_settings('default_shipping_price')->value;
                    }else{
                        $shippingFee = $shippingFee->price;
                    }
                }
                else{
                    $shippingFee = $shippingFee->price;
                }
            }else{
                $shippingFee = $shippingFee->price;
            }

            return view('frontend.payment_select', compact('total','shipping','min_order_free','shippingFee'));
        } else {
            flash(translate('Your Cart was empty'))->warning();
            return redirect()->route('home');
        }
    }

    public function get_payment_info(Request $request)
    {
        $subtotal = 0;
        $tax = 0;
        $shipping = $this->getShippingFee();
        foreach (Session::get('cart') as $key => $cartItem) {
            $subtotal += $cartItem['price'] * $cartItem['quantity'];
            $tax += $cartItem['tax'] * $cartItem['quantity'];
        }
        $total = $subtotal + $tax + $shipping;

        if (Session::has('coupon_discount')) {
            $total -= Session::get('coupon_discount');
        }

        return view('frontend.payment_select', compact('total','shipping'));
    }

    public function apply_coupon_code(Request $request)
    {
        //dd($request->all());
        $coupon = Coupon::where('code', $request->code)->first();

        if ($coupon != null) {
            if (strtotime(date('d-m-Y')) >= $coupon->start_date && strtotime(date('d-m-Y')) <= $coupon->end_date) {
                if (CouponUsage::where('user_id', Auth::user()->id)->where('coupon_id', $coupon->id)->first() == null) {
                    $coupon_details = json_decode($coupon->details);

                    if ($coupon->type == 'cart_base') {
                        $subtotal = 0;
                        $tax = 0;
                        $shipping = 0;
                        foreach (Session::get('cart') as $key => $cartItem) {
                            $subtotal += $cartItem['price'] * $cartItem['quantity'];
                            $tax += $cartItem['tax'] * $cartItem['quantity'];
                            $shipping += $cartItem['shipping'] * $cartItem['quantity'];
                        }
                        $sum = $subtotal + $tax + $shipping;

                        if ($sum >= $coupon_details->min_buy) {
                            if ($coupon->discount_type == 'percent') {
                                $coupon_discount = ($sum * $coupon->discount) / 100;
                                if ($coupon_discount > $coupon_details->max_discount) {
                                    $coupon_discount = $coupon_details->max_discount;
                                }
                            } elseif ($coupon->discount_type == 'amount') {
                                $coupon_discount = $coupon->discount;
                            }
                            $request->session()->put('coupon_id', $coupon->id);
                            $request->session()->put('coupon_discount', $coupon_discount);
                            flash(translate('Coupon has been applied'))->success();
                        }
                    } elseif ($coupon->type == 'product_base') {
                        $coupon_discount = 0;
                        foreach (Session::get('cart') as $key => $cartItem) {
                            foreach ($coupon_details as $key => $coupon_detail) {
                                if ($coupon_detail->product_id == $cartItem['id']) {
                                    if ($coupon->discount_type == 'percent') {
                                        $coupon_discount += $cartItem['price'] * $coupon->discount / 100;
                                    } elseif ($coupon->discount_type == 'amount') {
                                        $coupon_discount += $coupon->discount;
                                    }
                                }
                            }
                        }
                        $request->session()->put('coupon_id', $coupon->id);
                        $request->session()->put('coupon_discount', $coupon_discount);
                        flash(translate('Coupon has been applied'))->success();
                    }
                } else {
                    flash(translate('You already used this coupon!'))->warning();
                }
            } else {
                flash(translate('Coupon expired!'))->warning();
            }
        } else {
            flash(translate('Invalid coupon!'))->warning();
        }
        return back();
    }

    public function remove_coupon_code(Request $request)
    {
        $request->session()->forget('coupon_id');
        $request->session()->forget('coupon_discount');
        return back();
    }

    public function order_confirmed()
    {
        $order = Order::findOrFail(Session::get('order_id'));
        return view('frontend.order_confirmed', compact('order'));
    }

    public function getShippingFee()
    {
        $shipping = 0;
        $min_order_free = -1;
        if(!empty(get_settings('apply_district'))){
            $apply_district = get_settings('apply_district')->value;
        }

        if(!get_settings('free_order_mim')->disabled && !empty($apply_district) && in_array(session('shipping_info')['district_id'], unserialize($apply_district))){
            $min_order_free = get_settings('free_order_mim')->value;
        }

        $admin_products = [];
        $price_admin = 0;
        $seller_products = array();
        $seller_price = array();
        $price_item = 0;
        $admin_free = false;
        $admin_fast = false;
        $seller_fast = array();
        foreach (Session::get('cart') as $key => $cartItem) {
            if (empty($cartItem['shipping_type']) || $cartItem['shipping_type'] == 'home_delivery' || $cartItem['shipping_type'] == 'shipping_fast') {
                $product = \App\Product::find($cartItem['id']);
                if($product->added_by == 'admin'){
                    if(!empty($cartItem['shipping_type']) && $cartItem['shipping_type'] == 'shipping_fast'){
                        $admin_fast = true;
                    }
                    array_push($admin_products, $cartItem['id']);
                    $price_admin += $cartItem['price']*$cartItem['quantity'];
                }else{
                    $product_ids = array();
                    if(array_key_exists($product->user_id, $seller_products)){
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['price']*$cartItem['quantity']);
                    $seller_products[$product->user_id] = $product_ids;
                    if(!empty($cartItem['shipping_type']) && $cartItem['shipping_type'] == 'shipping_fast'){
                        $seller_fast[$product->user_id] = true;
                    }else{
                        $seller_fast[$product->user_id] = false;
                    }
                }
            }else{
                $product = \App\Product::find($cartItem['id']);
                if($product->added_by == 'admin'){
                    $admin_free = true;
                }else{
                    $product_ids = array();
                    if(array_key_exists($product->user_id, $seller_products)){
                        $product_ids = $seller_products[$product->user_id];
                    }
                    array_push($product_ids, $cartItem['price']*$cartItem['quantity']);
                    $seller_products[$product->user_id] = -1;
                }
            }
        }
        $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->where('ward_id', session('shipping_info')['ward_id'])->where('denied',0)->first();
        if (is_null($shippingFee)){
            $shippingFee = \App\TransportationArea::where('district_id', session('shipping_info')['district_id'])->whereNull('ward_id')->where('denied',0)->first();
            if (is_null($shippingFee)){
                $shippingFee = \App\Transportation::where('province_id', session('shipping_info')['province_id'])->first();
                if (is_null($shippingFee)){
                    $shippingFee = get_settings('default_shipping_price')->value;
                }else{
                    $shippingFee = $shippingFee->price;
                }
            }
            else{
                $shippingFee = $shippingFee->price;
            }
        }else{
            $shippingFee = $shippingFee->price;
        }

        if(!empty($admin_products) && !$admin_free){
            if($min_order_free == -1 || $price_admin < $min_order_free ){
                $shipping += $shippingFee;

            }
            if($admin_fast){
                $shipping += get_settings('price_shipping_fast')->value;
            }
        }
        if(!empty($seller_products)){
            foreach ($seller_products as $key => $seller_product) {
                if(!is_array($seller_product) && $seller_product == -1){
                    $shipping += 0;
                }else{
                    $seller_price = 0;
                    foreach ($seller_product as $price) {
                        $seller_price += $price;
                    }
                    if($min_order_free == -1 || $seller_price < $min_order_free ){
                        $shipping += $shippingFee;
                    }
                    if(!empty($seller_fast[$key])){
                        $shipping += get_settings('price_shipping_fast')->value;
                    }
                }

            }
        }
        return $shipping;
    }

    public function shippingFeeAjax(Request $request)
    {

        if (Session::has('cart') && count(Session::get('cart')) > 0) {
            $cart = $request->session()->get('cart', collect([]));
            $cart = $cart->map(function ($object, $key) use ($request) {
                if (\App\Product::find($object['id'])->added_by == 'admin') {
                    if ($request['shipping_type_admin'] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_admin'] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request->pickup_point_id_admin;
                    }
                } else {
                    if ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'home_delivery') {
                        $object['shipping_type'] = 'home_delivery';
                    }elseif ($request['shipping_type_' . \App\Product::find($object['id'])->user_id] == 'shipping_fast') {
                        $object['shipping_type'] = 'shipping_fast';
                    } else {
                        $object['shipping_type'] = 'pickup_point';
                        $object['pickup_point'] = $request['pickup_point_id_' . \App\Product::find($object['id'])->user_id];
                    }
                }
                return $object;
            });

            $request->session()->put('cart', $cart);

            $cart = $cart->map(function ($object, $key) use ($request) {
                $object['shipping'] = getShippingCost($key);
                return $object;
            });

            $request->session()->put('cart', $cart);

            $subtotal = 0;
            $tax = 0;
            $shipping = $this->getShippingFee();
            foreach (Session::get('cart') as $key => $cartItem) {
                $subtotal += $cartItem['price'] * $cartItem['quantity'];
                $tax += $cartItem['tax'] * $cartItem['quantity'];
            }
            $total = $subtotal + $tax + $shipping;

            if (Session::has('coupon_discount')) {
                $total -= Session::get('coupon_discount');
            }
            $content_wallet = '<div class="text-center bg-gray py-4">
                <i class="fa"></i>
                <div class="h5 mb-4">'.translate('Your wallet balance :').'<strong>'. single_price(Auth::user()->balance).'</strong></div>';
            if(Auth::user()->balance < $total){
                $content_wallet .= '<button type="button" class="btn btn-base-2" disabled>'. translate('Insufficient balance').'</button>';
            }else{
                $content_wallet .= '<button  type="button" onclick="use_wallet()" class="btn btn-base-1">'.translate('Pay with wallet').'</button>';
            }
            $content_wallet .= '</div>';
            return json_encode(['shipping_price'=>single_price($shipping),'total_price'=>single_price($total),'content_wallet'=>$content_wallet]);
        }
        return json_encode(['shipping_price'=>single_price(0),'total_price'=>single_price(0)]);
    }
}
