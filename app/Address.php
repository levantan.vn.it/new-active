<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    public function fk_district()
    {
        return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function fk_province()
    {
        return $this->hasOne(Province::class, 'id', 'province_id');
    }
}
