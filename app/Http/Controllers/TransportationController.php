<?php

namespace App\Http\Controllers;

use App\Province;
use App\District;
use App\Ward;
use App\Transportation;
use App\TransportationArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportationController extends Controller
{
    public function index()
    {
        $data = Transportation::all();
        $provinces = Province::all();

        return view('backend.transportations.index', compact('data', 'provinces'));
    }

    public function store(Request $request)
    {
        $check = Transportation::where('province_id', $request->province_id)->first();
        if(!empty($check)){
            flash(translate('Transportations has been already exist'))->error();
            return back();
        }
        $data = new Transportation;
        $data->province_id = $request->province_id;
        $data->name = $request->name;
        $data->price = str_replace(',', '', $request->price);
        $data->save();

        // $districts = District::where('province_id', $request->province_id)->get();
        // foreach($districts as $district) {
        //     $area = new TransportationArea;
        //     $area->transport_id = $data->id;
        //     $area->province_id = $request->province_id;
        //     $area->district_id = $district->id;
        //     $area->price = $data->price;
        //     $area->save();
        // }
        flash(translate('Transportations has been inserted successfully'))->success();
        return back();
    }

    public function storeAdress(Request $request)
    {
        if(!empty($request->ward_id)){
            $check = TransportationArea::where('district_id', $request->district_id)->where('ward_id', $request->ward_id)->first();
            if(!empty($check)){
                flash(translate('Transportations has been already exist'))->error();
                return back();
            }
        }else{
            $check = TransportationArea::where('district_id', $request->district_id)->whereNull('ward_id')->first();
            if(!empty($check)){
                flash(translate('Transportations has been already exist'))->error();
                return back();
            }
        }
        $area = new TransportationArea;
        $area->transport_id = $request->transport_id;
        $area->province_id = $request->province_id;
        $area->district_id = $request->district_id;
        $area->ward_id = $request->ward_id;
        $area->price = str_replace(',', '', $request->price);;
        $area->save();
        flash(translate('Transportations has been inserted successfully'))->success();
        return back();
    }

    public function update(Request $request, $id)
    {
        $data = Transportation::find($id);
        $data->name = $request->name;
        $data->price = str_replace(',', '', $request->price);
        $data->save();

        if($id > 0) {
            foreach($data->fk_transportation_areas as $area){
                $area = TransportationArea::find($area->id);
                $area->district_id = $request->district_id[$area->id];
                $area->price = str_replace(',', '', $request->district_price[$area->id]);
                $area->denied = !empty($request->district_deny[$area->id]) ? true : false;
                $area->save();
            }
        }
        flash(translate('Transportations has been updated successfully'))->success();
        return back();
    }

    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            Transportation::destroy($id);
            TransportationArea::where('transport_id',$id)->delete();
            return response()->json([
                'alert' => 'success',
                'title' => 'Completed',
                'msg' => 'Transportations has been deleted successfully.'
            ]);
        }
    }

    public function destroyArea(Request $request, $id)
    {
        if($request->ajax()){
            TransportationArea::where('id',$id)->delete();
            return response()->json([
                'alert' => 'success',
                'title' => 'Completed',
                'msg' => 'Transportation area has been deleted successfully.'
            ]);
        }
    }

    public function setting(Request $request)
    {
        $setting = \App\Setting::where('key', 'default_shipping_price')->first();
        if($request->has('default_shipping_price'))
            $setting->value = str_replace(',', '', $request->default_shipping_price);
        $setting->disabled = $request->disabled ? true : false;
        $setting->save();
        $setting2 = \App\Setting::where('key', 'free_order_mim')->first();
        if($request->has('free_order_mim'))
            $setting2->value = str_replace(',', '', $request->free_order_mim);
        $setting2->disabled = $request->disabled_free_order_mim ? true : false;
        $setting2->save();
        $setting = \App\Setting::where('key', 'apply_district')->first();
        if($request->has('apply_district'))
            $setting->value = serialize($request->apply_district);
        else
            $setting->value = "";
        $setting->save();

        $setting = \App\Setting::where('key', 'price_shipping_fast')->first();
        if($request->has('price_shipping_fast'))
            $setting->value = str_replace(',', '', $request->price_shipping_fast);
        else
            $setting->value = 0;
        $setting->save();
        flash(translate('Shipping settings saved successfully'))->success();
        return back();
    }
}
