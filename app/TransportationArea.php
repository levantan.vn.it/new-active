<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransportationArea extends Model
{
    protected $table = 'transportation_areas';

    public function fk_transportation()
    {
    	return $this->hasOne(Transportation::class, 'id', 'transport_id');
    }

    public function fk_district()
    {
    	return $this->hasOne(District::class, 'id', 'district_id');
    }

    public function fk_ward()
    {
        return $this->hasOne(Ward::class, 'id', 'ward_id');
    }

    public function fk_province()
    {
    	return $this->hasOne(Province::class, 'id', 'province_id');
    }
}
